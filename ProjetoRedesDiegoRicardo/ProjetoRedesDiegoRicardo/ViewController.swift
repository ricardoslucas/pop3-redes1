//
//  ViewController.swift
//  ProjetoRedesDiegoRicardo
//
//  Created by Diego Azevedo Vasconcelos on 23/04/19.
//  Copyright © 2019 Diego Azevedo Vasconcelos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var addreess = ProXimityAPIClient()
    
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var portLabel: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var portTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passworldTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.configure()
    }

    func configure() {
        
        self.addressLabel.text = "Host"
        self.portLabel.text = "Port"
        self.email.text = "Email"
        self.passwordLabel.text = "Senha"
        
    }
    
    @IBAction func connectAction(_ sender: Any) {
        
        if addressTextField.text!.count > 0, portTextField.text!.count > 0, emailTextField.text!.count > 0, passworldTextField.text!.count > 0 {
        
            let host = addressTextField.text!
            let port = Int(portTextField.text!)
            
            addreess.connect(host: host, port: port!)
        } else {
            
            let alert = UIAlertController.init(title: "ERRO", message: "Algum campo está vavio", preferredStyle: .alert)
            
            alert.perform(nil)
            
        }
    }
    
    func verificacoes() {
        
        
        let output = addreess.outputStream.debugDescription
        print(output)
        
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }

}
